let botonRojo = document.querySelectorAll('button')[0];
let botonVerde = document.querySelectorAll('button')[1];
let botonAzul = document.querySelectorAll('button')[2];

// Sin función anónima
botonRojo.addEventListener("click", color);

// Con función anónima
botonVerde.addEventListener("click", function () {
    cambiarColor('green');
});

botonAzul.addEventListener("click", color);

function color(evento) {
    //boton que se ha pulsado
    let boton = evento.target;
    //nombre del color que lo sacamos del atributo del HTML
    let nombreColor = boton.getAttribute("data-color");

    const caja = document.querySelector('div');

    caja.style.backgroundColor = nombreColor;
}


function cambiarColor(color) {
    const caja = document.querySelector('div');

    caja.style.backgroundColor = color;
}
