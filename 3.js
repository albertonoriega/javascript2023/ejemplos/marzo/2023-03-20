// constante que apunte a todos los td
const celdas = document.querySelectorAll('td');

// Bucle para cambiar todas las celdas de la tabla
for (let i = 0; i < celdas.length; i++) {

    celdas[i].addEventListener("click", function (evento) {
        // Array con diferentes colores
        let colores = ["black", "red", "blue", "green", "gold", "yellow", "purple", "silver", "grey"];
        // generamos dos numeros aleatorios del 0 al 8
        let numeroFondo = parseInt(Math.random() * 8);
        let numeroTexto = parseInt(Math.random() * 8);

        // Evitamos que el fondo y el texto tengo el mismo color
        if (numeroFondo == numeroTexto) {
            numeroTexto = parseInt(Math.random() * 8);
        }

        // Seleccionamos el elemento
        const elemento = evento.target;
        // cambiamos los estilos de cada td aleatoriamente
        elemento.style.backgroundColor = colores[numeroFondo];
        elemento.style.color = colores[numeroTexto];
        elemento.style.textAlign = "center"
    });
}



